#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NBRCOLONNE 40
#define NBRLIGNE 20
#define ERREURPARAM "Erreur: Attendu un seul argument: le nombre de jours à simuler.\n"
#define ERREURINVALIDPARAM "Erreur: Le nombre de jours à simuler doit être entre 0 et 100.\n"
#define ERREUREOF "Erreur: Caractère `EOF` inattendu, attendu `H`, `X` ou `.`.\n"

//PROTOTYPES
void prendreGrille(char grille[NBRLIGNE][NBRCOLONNE]);
void afficherGrille(char grille[NBRLIGNE][NBRCOLONNE], int jour);
void traiterProchainJour(char grilleEntree[NBRLIGNE][NBRCOLONNE], char grilleSortie[NBRLIGNE][NBRCOLONNE]);
void verifierVoisins(char grille[NBRLIGNE][NBRCOLONNE],char grilleFinale[NBRLIGNE][NBRCOLONNE], int posX, int posY);
void testerPositionVoisin(char grille[NBRLIGNE][NBRCOLONNE], int position, int posY, int posX);

//VARIABLES GLOBALES
int infecte;
int saine;
int vide;

int main(int argc, char* argv[]){
	char grid1[NBRLIGNE][NBRCOLONNE];
	char grid2[NBRLIGNE][NBRCOLONNE];
	int nbrDeJours = 0;
	int i;

	if(argc > 2)
	{
		printf(ERREURPARAM);
		exit(1);
	}
	
	if(argc == 2)
	{
		for(i = 0; i<strlen(argv[1]); i++)
        	{
                	if(isdigit(argv[1][i])==0)
                	{
                        	printf(ERREURINVALIDPARAM);
				exit(1);
                	}
        	}

		nbrDeJours = atoi(argv[1]);
		if(nbrDeJours < 0 || nbrDeJours > 100)
		{
			printf(ERREURINVALIDPARAM);
			exit(1);
		}
	}


	prendreGrille(grid1);
	afficherGrille(grid1, 0);

	for(i = 0; i< nbrDeJours; ++i)
	{
		if(i % 2 == 1)
		{
			traiterProchainJour(grid2, grid1);
        		afficherGrille(grid1, i+1);
		}
		else if(i % 2 == 0)
		{
			traiterProchainJour(grid1, grid2);
                        afficherGrille(grid2, i+1);
		}
	}
	return 0;
}

/**
 * Fait la saisie de la grille par l'utilisateur et la place dans le paramètre grille
 * @param  grille  grille 20x40 où les données sont stockées
*/
void prendreGrille(char grille[NBRLIGNE][NBRCOLONNE]){
	char myString[NBRLIGNE*NBRCOLONNE];
	char input;
	int total = 0;
	int filler = 0;
	while((input = getchar()) != EOF && total < NBRLIGNE*NBRCOLONNE)
	{
		if(input != 'X' && input != 'H' && input != '.' && input != ' ' && input != '\n')
		{
			printf("Erreur: Caractère `%c` inattendu, attendu `H`, `X` ou `.`.\n", input);
			exit(1);
		}
		else if(input == 'X' || input == 'H' || input == '.'){
			grille[total/NBRCOLONNE][total%NBRCOLONNE]=input;
			total++;
		}
	}

	if(input == EOF){
		printf(ERREUREOF);
		exit(1);
	}	
}

/**
 * Affiche la grille ainsi que le jour spécifié en paramètre dans la sortie standard de la console
 * @param  grille  grille 20x40 d'où les données sont affichées
 * @param  jour    numéro de jour de la simulation
*/
void afficherGrille(char grille[NBRLIGNE][NBRCOLONNE], int jour)
{
	int i, j;
	printf("Jour %d\n", jour);
	for(i = 0; i<NBRLIGNE; ++i)
	{
		for(j = 0; j<NBRCOLONNE; ++j)
		{
			printf("%c", grille[i][j]);
		}
		printf("\n");
	}
}

/**
 * Parcourt la matrice grilleEntree et affecte le résultat de la fonction verifierVoisins dans la grilleSortie
 * @param grilleEntree  grille 20x40 servant de données d'entrée
 * @param grilleSortie  grille 20x40 servant à stocker les données finales
*/
void traiterProchainJour(char grilleEntree[NBRLIGNE][NBRCOLONNE], char grilleSortie[NBRLIGNE][NBRCOLONNE]){
	int i, j;
	
	for(i = 0; i<NBRLIGNE; ++i)
	{
		for(j = 0; j<NBRCOLONNE; j++)
		{
			verifierVoisins(grilleEntree, grilleSortie, i, j);
		}
	}	
}

/**
 * Détermine le destin de la case à la position (posY, posX) de la grille à l'aide de la fonction testerPositionVoisin ainsi que des voisins appropriés
 * @param  grille        grille 20x40 servant de données d'entrée
 * @param  grilleFinale  grille 20x40 servant à stocker les données finales
 * @param  posY          coordonné de la ligne de l'élément
 * @param  posX          coordonné de la colonne de l'élément
*/
void verifierVoisins(char grille[NBRLIGNE][NBRCOLONNE], char grilleFinale[NBRLIGNE][NBRCOLONNE], int posY, int posX)
{
	infecte = 0;
	saine = 0;
	vide = 0;

	if(posY == 0)
	{
		if(posX == 0)
		{
			testerPositionVoisin(grille, 5, posY, posX);
			testerPositionVoisin(grille, 7, posY, posX);
			testerPositionVoisin(grille, 8, posY, posX);
		}
		else if(posX == NBRCOLONNE-1)
                {
			testerPositionVoisin(grille, 4, posY, posX);
                        testerPositionVoisin(grille, 6, posY, posX);
                        testerPositionVoisin(grille, 7, posY, posX);
                }
		else
		{
			testerPositionVoisin(grille, 4, posY, posX);
                        testerPositionVoisin(grille, 5, posY, posX);
                        testerPositionVoisin(grille, 6, posY, posX);
                        testerPositionVoisin(grille, 7, posY, posX);
                        testerPositionVoisin(grille, 8, posY, posX);

		}
	}
	else if(posY == NBRLIGNE-1)
	{
		if(posX == 0)
                {
                        testerPositionVoisin(grille, 2, posY, posX);
                        testerPositionVoisin(grille, 3, posY, posX);
                        testerPositionVoisin(grille, 5, posY, posX);
                }
                else if(posX == NBRCOLONNE-1)
                {
                        testerPositionVoisin(grille, 1, posY, posX);
                        testerPositionVoisin(grille, 2, posY, posX);
                        testerPositionVoisin(grille, 4, posY, posX);
                }
                else
                {
                        testerPositionVoisin(grille, 1, posY, posX);
                        testerPositionVoisin(grille, 2, posY, posX);
                        testerPositionVoisin(grille, 3, posY, posX);
                        testerPositionVoisin(grille, 4, posY, posX);
                        testerPositionVoisin(grille, 5, posY, posX);
                }
	}
	else if(posX == 0)
	{
		testerPositionVoisin(grille, 2, posY, posX);
		testerPositionVoisin(grille, 3, posY, posX);
		testerPositionVoisin(grille, 5, posY, posX);
		testerPositionVoisin(grille, 7, posY, posX);
		testerPositionVoisin(grille, 8, posY, posX);
	}
	else if(posX == NBRCOLONNE-1)
	{
		testerPositionVoisin(grille, 1, posY, posX);
		testerPositionVoisin(grille, 2, posY, posX);
		testerPositionVoisin(grille, 4, posY, posX);
		testerPositionVoisin(grille, 6, posY, posX);
		testerPositionVoisin(grille, 7, posY, posX);
	}
	else
	{
		testerPositionVoisin(grille, 1, posY, posX);
                testerPositionVoisin(grille, 2, posY, posX);
                testerPositionVoisin(grille, 3, posY, posX);
                testerPositionVoisin(grille, 4, posY, posX);
                testerPositionVoisin(grille, 5, posY, posX);
		testerPositionVoisin(grille, 6, posY, posX);
		testerPositionVoisin(grille, 7, posY, posX);
		testerPositionVoisin(grille, 8, posY, posX);

	}

	grilleFinale[posY][posX] = grille[posY][posX];

	if(grille[posY][posX] == '.')
	{
		if(infecte + saine == 3)
		{
			if(infecte > saine)
				grilleFinale[posY][posX] = 'X';
			else
				grilleFinale[posY][posX] = 'H';
		}
		else
			grilleFinale[posY][posX] = '.';
		return;
	}
	else if((infecte + saine) >= 4 || (infecte + saine) < 2)
		grilleFinale[posY][posX] = '.';
	else if(infecte + saine == 2 || infecte + saine == 3)
	{
		if(infecte > saine)
			grilleFinale[posY][posX] = 'X';
		else
			grilleFinale[posY][posX] = 'H';
	}
}

/**
 * Note la quantité de population infectée, saine et absente à la position spécifiée de l'élément dans la grille à l'index [posY][posX]
 * @param  grille    grille 20x40 servant de données d'entrée
 * @param  position  détermine quel élément parcourir dans le tableau par rapport à l'élément en traîtement. 
 * 		     Fonctionne selon la matrice 3x3 centralisée sur l'élément en traîtement.
 * @param  posY      coordonné de la ligne de l'élément
 * @param  posX      coordonné de la colonne de l'élément
*/
void testerPositionVoisin(char grille[NBRLIGNE][NBRCOLONNE], int position, int posY, int posX)
{
	if(position == 1)
	{
		if(grille[posY-1][posX-1] == 'X')
			infecte++;
		else if(grille[posY-1][posX-1] == 'H')
			saine++;
		else
			vide++;
	}
	else if(position == 2)
	{
		if(grille[posY-1][posX] == 'X')
                        infecte++;
                else if(grille[posY-1][posX] == 'H')
                        saine++;
                else
                        vide++;
	}
	else if(position == 3)
	{
		if(grille[posY-1][posX+1] == 'X')
                        infecte++;
                else if(grille[posY-1][posX+1] == 'H')
                        saine++;
                else
                        vide++;
	}
	else if(position == 4)
	{
		if(grille[posY][posX-1] == 'X')
                        infecte++;
                else if(grille[posY][posX-1] == 'H')
                        saine++;
                else
                        vide++;
	}
	else if(position == 5)
	{
		if(grille[posY][posX+1] == 'X')
                        infecte++;
                else if(grille[posY][posX+1] == 'H')
                        saine++;
                else
                        vide++;
	}
	else if(position == 6)
	{
		if(grille[posY+1][posX-1] == 'X')
                        infecte++;
                else if(grille[posY+1][posX-1] == 'H')
                        saine++;
                else
                        vide++;
	}
	else if(position == 7)
	{
		if(grille[posY+1][posX] == 'X')
                        infecte++;
                else if(grille[posY+1][posX] == 'H')
                        saine++;
                else
                        vide++;
	}
	else if(position == 8)
	{
		if(grille[posY+1][posX+1] == 'X')
                        infecte++;
                else if(grille[posY+1][posX+1] == 'H')
                        saine++;
                else
                        vide++;
	}
}
