# TP1: `pandemi.c`

## Description

Pandemic est un programme de simulation de la propagation d'un virus dans une réalité fictive de la Terre. Le programme utilise une grille de 20x40 représentant la population mondiale. Il détermine l'évolution par jour de la population ainsi que du virus dans celle-ci. Il est réalisé dans le cadre du cours de INF3135 Construction et maintenance de logiciels, enseigné par Alexandre Blondin Massé et Alexandre Terrasa à l'automne 2017, à l'Université du Québec à Montréal.

## Auteur

Louis-Philippe Geoffrion GEOL15129306

## Fonctionnement

Le programme se compile avec la cible `all` du Makefile
```sh
make
```
On peut par la suite le lancer avec la commande
```sh
bin/pandemic <jour>
<grille>
```
où jour représente la quantité de jours de prédiction du programme, entre 0 et 100, et où grille représente l'insertion d'une grille 20x40 de caractères valides (X pour population infectée, H pour population saine et . pour absence de population) 

## Tests

En tout temps, il est possible de tester le programme en lançant la commande

```sh
./tests.sh
```

ou à l'aide de la cible `check` du Makefile

```sh
make check
```

**Remarque:** Vous devez d'abord implémenter la cible `bin/pandemic` dans le
fichier Makefile pour utiliser le script de tests.

### Format des tests

Le script de tests s'occupe de compiler le programme `pandemic` en utilisant le
Makefile puis de l'exécuter avec les tests trouvés dans le répertoire `tests/`.

Le format des cas de tests est le suivant:

* `test_XX.args` nombre de jour à simuler à passer en argument au programme
* `test_XX.in` carte à saisir dans `stdin`
* `test_XX.res` sortie attendue (`stdout` et `stderr` confondus)

### Générateur de cartes

Le programme `generator` permet de générer des cartes de configuration aléatoirement.
Il est très pratique pour tester votre programme.

Usage:
```sh
Usage: generator <lignes> <colonnes>
```

Exemple d'utilisation:
```sh
make bin/generator
bin/generator 5 10
```

Affiche:
```sh
..X...HX..
..XXX...HH
H..XH.H...
.XHXXXXX..
...HXXH.X.
```

## Références

* [Documentation officielle du TP](https://gitlab.com/ablondin/inf3135-aut2017-tp1-enonce)
* [Documentation Markdown de Wordpress](https://en.support.wordpress.com/markdown-quick-reference/)
* [Documentation de cours - Alexandre Terrasa](http://moz-code.org/uqam/cours/INF3135/)

## État du projet

Indiquez toutes les tâches qui ont été complétés en insérant un `X` entre les
crochets. Si une tâche n'a pas été complétée, expliquez pourquoi.

* [X] Le nom du dépôt GitLab est exactement `inf3135-aut2017-tp1` (Pénalité de
  **50%**).
* [X] L'URL du dépôt GitLab est exactement (remplacer `utilisateur` par votre
  nom identifiant GitLab) `https://gitlab.com/paindeviande/inf3135-aut2017-tp1`
  (Pénalité de **50%**).
* [X] L'utilisateur `ablondin` (groupe 20) ou `Morriar` (groupe 40) a accès au
  projet en mode *Developer* (Pénalité de **50%**).
* [X] Le dépôt GitLab est privé (Pénalité de **50%**).
* [X] Le fichier `.gitignore` a été mis à jour.
* [X] Le fichier `pandemi.c` a été implémenté.
* [X] La cible `bin/pandemic` a été ajoutée dans le Makefile.
* [X] Aucun test exécuté avec la commande `make check` n'échoue.
* [X] Les sections incomplètes du fichier `README.md` ont été complétées.
